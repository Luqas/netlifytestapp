package org.netlify;

import org.netlify.body.Modal;
import org.netlify.pages.HomePage;

public class NetlifyTestApp {
    public static final String DEMO_SHOP_TITLE = " Demo shop ";


    public static void main(String[] args) {

        HomePage homePage = new HomePage ();
        homePage.verifyThatTitleIsDisplayedOnScreen ();

        homePage.validateThatHeaderContainsLogoIcon ();
        homePage.validateThatHeaderContainsShoppingCartIcon ();
        homePage.validateThatHeaderContainsWishlistIcon ();
        homePage.validateThatHeaderContainsGreetingsMessage ();
        homePage.validateThatHeaderContainsLoginButton ();

        homePage.validateThatFooterContainsDetails ();
        homePage.validateThatFooterContainsQuestionIcon ();
        homePage.validateThatFooterContainsResetIcon ();

        homePage.clickOnTheLoginButon ();
        Modal modal = new Modal ();
        modal.validateThatModalIsDisplayed ();
        modal.validateModalTitle ();
        modal.validateCloseButton ();
        modal.validateUsernameFieldIsDisplayed();
        modal.validatePasswordFieldIsDisplayed();
        modal.validateThatLoginButtonIsDisplayed();
        modal.validateThatLoginButtonIsEnabled();
        modal.clickOnTHeCloseButton();
        homePage.validateModalIsNotDisplayed();

        



    }
}