package org.netlify.body;

public class Footer {

    private final String details;
    private final String questionIcon;
    private final String resetIcon;


    public Footer() {
        this.details = "Demo Shop | build date 2021-05-21 14:04:30 GTBDT";
        this.questionIcon = "?";
        this.resetIcon = "Reset the application state";
    }

    public String getDetails() {
        return details;
    }

    public String getQuestionIcon() {
        return questionIcon;
    }

    public String getResetIcon() {
        return resetIcon;
    }
}
