package org.netlify.body;

public class Modal {

    private final String modalTitle;
    private final String closeButton;
    private final String username;
    private final String password;
    private final String loginButton;

    public Modal() {
        this.modalTitle = "Login";
        this.closeButton = "x";
        this.username = "user-name";
        this.password = "password";
        this.loginButton = "sign-in-alt";
    }

    public String getModalTitle() {
        return modalTitle;
    }

    public String getCloseButton() {
        return closeButton;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getLoginButton() {
        return loginButton;
    }

    public void validateThatModalIsDisplayed() {
        System.out.println ("Verify that modal is displayed.");

    }


    public void validateModalTitle() {
        System.out.println ("Verify that modal title is : "+modalTitle);
    }

    public void validateCloseButton() {
        System.out.println ("Verify that modal close button is : "+closeButton);
    }

    public void validateUsernameFieldIsDisplayed() {
        System.out.println ("Verify that username field is displayed.");
    }

    public void validatePasswordFieldIsDisplayed() {
        System.out.println ("Verify that password field is displayed.");
    }

    public void validateThatLoginButtonIsDisplayed() {
        System.out.println ("Verify that login button is : "+loginButton);
    }

    public void validateThatLoginButtonIsEnabled() {
        System.out.println ("Verify that login button is enabled.");
    }

    public void clickOnTHeCloseButton() {
        System.out.println ("Clicked on the "+closeButton+ " button. ");

    }
}
