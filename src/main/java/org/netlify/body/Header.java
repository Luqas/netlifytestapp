package org.netlify.body;

public class Header {

    private final String logoIcon;
    private final String shoppingCartIcon;
    private final String wishListIcon;
    private final String greetingsMessage;
    private final String loginButton;


    public Header() {
        this.logoIcon = "/";
        this.shoppingCartIcon = "/cart";
        this.wishListIcon = "/wishlist";
        this.greetingsMessage = "Hello guest !";
        this.loginButton = "sign-in";
    }

    public String getLogoIcon() {
        return logoIcon;
    }

    public String getShoppingCartIcon() {
        return shoppingCartIcon;
    }

    public String getWishListIcon() {
        return wishListIcon;
    }

    public String getGreetingsMessage() {
        return greetingsMessage;
    }

    public String getLoginButton() {
        return loginButton;
    }

    /**
     * Actions
     */
    public void clickOnTheLoginBUtton() {
        System.out.println("               ----------");
        System.out.println ("Clicked on the login button.");

    }
}