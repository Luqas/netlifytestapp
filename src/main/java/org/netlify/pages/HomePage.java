package org.netlify.pages;

import org.netlify.NetlifyTestApp;
import org.netlify.body.Footer;
import org.netlify.body.Header;

public class HomePage {
    private final String title = NetlifyTestApp.DEMO_SHOP_TITLE;
    private final Header header;
    private final Footer footer;
    public HomePage() {
        this.header = new Header();
        this.footer = new Footer();
    }

    public void verifyThatTitleIsDisplayedOnScreen() {
        System.out.println("Verify that title is : "+title);
    }

    public void validateThatHeaderContainsLogoIcon() {
        System.out.println("               ----------");
        System.out.println("Verify that logo is : "+ header.getLogoIcon());
    }

    public void validateThatHeaderContainsShoppingCartIcon() {
        System.out.println("Verify that shopping cart is : "+ header.getShoppingCartIcon());
    }

    public void validateThatHeaderContainsWishlistIcon() {
        System.out.println("Verify that wishlist is : "+ header.getWishListIcon());
    }

    public void validateThatHeaderContainsGreetingsMessage() {
        System.out.println("Verify that greetings message is : "+ header.getGreetingsMessage());

    }

    public void validateThatHeaderContainsLoginButton() {
        System.out.println("Verify that login button is : "+ header.getLoginButton());
        System.out.println("               ----------");
    }

    public void validateThatFooterContainsDetails() {
        System.out.println("Verify that details is : " +footer.getDetails());
    }

    public void validateThatFooterContainsQuestionIcon() {
        System.out.println("Verify that question icon is : "+ footer.getQuestionIcon());
    }

    public void validateThatFooterContainsResetIcon() {
        System.out.println("Verify that reset icon is : "+ footer.getResetIcon());
    }

    public void clickOnTheLoginButon() {
        this.header.clickOnTheLoginBUtton();
    }

    public void validateModalIsNotDisplayed() {
        System.out.println ("Verify that modal is not on page. ");
    }
}
