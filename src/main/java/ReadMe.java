public class ReadMe {
    /**
     * Am creat proiect nou , NetlifyTestApp, am schimbat in pom.xml si org.example numele in org.netlify
     * clasa Main devine NetlifyTestApp
     * am creat 2 package-uri in org.netlify , body si pages , in ele punem urmatoarele clase pe care le vom face
     * punem titlul , pe care il gasim cu inspect in HTML
     *
     *  public class NetlifyTestApp {
     *     public static final String DEMO_SHOP_TITLE = " Demo shop ";
     *
     * in pages facem clasa HomePage
     * aducem HomePage in NetlifyTestApp :
     *            HomePage homePage = new HomePage();
     * -verificam titlul
     *            homePage.verifyThatTitleIsDisplayedOnScreen();
     * pe HomePage avem  :
     *             public void verifyThatTitleIsDisplayedOnScreen() {
     *         System.out.println("Verify that title is "+title);
     *     }
     * - in package body facem 2 clase pe care vom verifica elementele de pe fiecare
     *
     * - Header - avem 5 elemente :
     *  - posetuta                                                 logoIcon
     *  - caruciorul                                               shoppingCartIcon
     *  - inima                                                    wishlistIcon
     *  - Hello guest!                                             greetingsMessage
     *  - sageata                                                  loginButton
     *
     * -pe Header facem elementele de mai sus :
     *
     *       private final String logoIcon;
     *       private final String shoppingCartIcon;
     *       private final String wishListIcon;
     *       private final String greetingsMessage;
     *       private final String loginButton;
     *
     *       Alt + Enter pe fiecare , Initialize in Constructor si obtinem ce se vede mai jos ,
     *       cu specificatia ca tot ce e dupa egal la fiecare e luat din HTML cu Inspect
     *
     *  public Header() {
     *         this.logoIcon = "/";
     *         this.shoppingCartIcon = "/cart";
     *         this.wishListIcon = "/wishlist";
     *         this.greetingsMessage = "Hello guest !";
     *         this.loginButton = "sign-in";
     *     }
     * - le facem cate un getter la fiecare si obtinem :
     *
     *          public String getLogoIcon() {
     *         return logoIcon;
     *     }
     *
     *     public String getShoppingCartIcon() {
     *         return shoppingCartIcon;
     *     }
     *
     *     public String getWishListIcon() {
     *         return wishListIcon;
     *     }
     *
     *     public String getGreetingsMessage() {
     *         return greetingsMessage;
     *     }
     *
     *     public String getLoginButton() {
     *         return loginButton;
     *     }
     *   pe NetlifyTestApp :
     *
     *    homePage.validateThatHeaderContainsLogoIcon();
     *    Alt + Enter , create metod pe HomePage , va trebui sa aducem clasa Header pe HomePage cu :
     *    private final Header header;
     *    Alt + Enter , constructor , rezulta
     *     this.header = new Header();
     * - acum putem valida functia :
     *           public void validateThatHeaderContainsLogoIcon() {
     *             System.out.println("Verify that logo is : "+ header.getLogoIcon());
     *             }
     * - facem la fel si celelalte elemente de pe Header
     *
     *
     *
     *  - Footer
     *
     *  - avem 3 elemente :
     *
     *   -textul cu Demo Shop | build date 2021-05-21 14:04:30 GTBDT     details
     *   -semnul intrebarii                                              questionIcon
     *   -sageata rotunda                                                resetIconTitle
     *
     *   facem elementele pe Footer, le construim :
     *
     *                      private final String details;
     *                      private final String questionIcon;
     *                      private final String resetIconTitle;
     *
     *
     *       public Footer() {
     *                     this.details = "Demo Shop | build date 2021-05-21 14:04:30 GTBDT";
     *                     this.questionIcon = "?";
     *                     this.resetIconTitle = "Reset the application state";
     *     }
     *     le facem si getter :
     *                          public String getDetails() {
     *         return details;
     *     }
     *
     *     public String getQuestionIcon() {
     *         return questionIcon;
     *     }
     *
     *     public String getResetIconTitle() {
     *         return resetIconTitle;
     *     }
     *
     *     la fel ca si la Header , aducem Footer pe HomePage si validam elementele din Footer
     *
     */


}
